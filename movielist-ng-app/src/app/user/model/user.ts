export interface User {
  id?: string;
  email: string;
  username: string;
  password: string;
  confirmPassword: string;
  isConfirmed?: boolean;
  activationCode?: string;
  roles?: [{role: string, description: string}];
}
