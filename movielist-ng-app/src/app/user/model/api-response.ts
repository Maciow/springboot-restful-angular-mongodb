export class ApiResponse {
  status: number;
  messages: Array<string>;
  result: any;
}
