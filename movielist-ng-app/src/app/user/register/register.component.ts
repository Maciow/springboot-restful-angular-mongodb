import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../service/user.service';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  passwordForm: FormGroup;
  errorMessages: Array<string>;

  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required,
        Validators.minLength(4),
        Validators.maxLength(12)],
        this.validateUsernameNotTaken.bind(this)],
      email: ['', [Validators.required,
        this.validateEmailPattern
      ], this.validateEmailNotTaken.bind(this)]
    });
    this.passwordForm = this.formBuilder.group({
      password: ['', [Validators.required,
        Validators.minLength(4),
        Validators.maxLength(12)]],
      confirmPassword: ['', [Validators.required]]
    }, {validator: this.validatePasswords.bind(this) });
    this.errorMessages = [];
  }

  onSubmit() {
    let userObject = this.getUserObject();
    this.userService.saveUser(userObject).subscribe(
      data => {
        if (data.status === 201) {
          this.router.navigate(['login']);
        } else {
          this.errorMessages = data.messages;
        }
      }
    );
  }

  private getUserObject() {
   return Object.assign(this.registerForm.value, this.passwordForm.value);
  }

  private validateEmailNotTaken(control: FormControl) {
    if (control.value.includes('@')) {
      return this.userService.isEmailExists(control.value).pipe(map(data => {
        return !data ? null : { emailTaken: true };
      }));
    }
    return null;
  }

  private validateUsernameNotTaken(control: FormControl) {
    let username = control.value;
    if (username.length > 3 && username.length < 13) {
      return this.userService.isUsernameExists(username).pipe(map(data => {
        return !data ? null : { usernameTaken: true };
      }));
    }
  }

  private validateEmailPattern(control: FormControl) {
    const EMAIL_REGEXP: RegExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return EMAIL_REGEXP.test(control.value) ? null : {
      validateEmailPattern: {
        valid: false
      }
    };
  }

  private validatePasswords(group: FormGroup) {
    let password = group.controls.password.value;
    let confirmPassword = group.controls.confirmPassword.value;

    if (password.length > 0 && password !== confirmPassword) {
      return { notSame: true };
    } else {
      return null;
    }
  }
}
