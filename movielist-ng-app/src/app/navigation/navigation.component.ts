import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../service/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private router: Router, private authService: AuthenticationService) { }

  private isLogged = false;
  private isAdmin = false;
  private isCollapsed: boolean;

  ngOnInit() {
    this.isCollapsed = false;
    this.authService.loggedCheck.subscribe(data => this.isLogged = data);
    this.authService.adminCheck.subscribe(data => this.isAdmin = data);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['movies']);
  }
}
