import { Component, OnInit } from '@angular/core';
import {Movie} from '../model/movie';
import {MovieService} from '../../service/movie.service';
import {ActivatedRoute} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-list-movie',
  templateUrl: './list-movie.component.html',
  styleUrls: ['./list-movie.component.css']
})
export class ListMovieComponent implements OnInit {

  private movies: Array<Movie> = [];
  private page;
  private size;
  private minRate;
  private genre;
  private totalPages: Array<number>;
  private totalRatings: Array<number>;
  private genresList: Array<string>;
  private isCollapsed: boolean;

  constructor(private movieService: MovieService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.page = 1;
    this.size = 5;
    this.minRate = 0;
    this.genre = null;
    this.totalRatings = new Array<number>(10);
    this.isCollapsed = false;
    this.genresList = ['Action', 'Adventure', 'Animation',
      'Documentary', 'Drama', 'Comedy',
      'Crime', 'Family', 'Horror', 'Melodrama',
      'Romance', 'Sci-Fi', 'Thriller'];
    this.totalPages = [];
    this.getAllMoviesFilter();
  }

  private getAllMoviesFilter() {

    this.route.queryParams.subscribe(data => {
      if (typeof data['page'] !== 'undefined') {
        this.page = data['page'];
      } else {
        this.page = 1;
      }
      if (typeof data['size'] !== 'undefined') {
        this.size = data['size'];
      } else {
        this.size = 5;
      }
      if (typeof data['minRate'] !== 'undefined') {
          this.minRate = data['minRate'];
      } else {
        this.minRate = 0;
      }
      if (typeof data['genres'] !== 'undefined') {
        this.genre = data['genres'];
      } else {
        this.genre = null;
      }

      let params = new HttpParams()
        .set('page', this.page.toString())
        .set('size', this.size.toString())
        .set('minRate', this.minRate.toString())
        .set('genres', this.genre);

      this.movieService.getAllMovies(params).subscribe(
        data => {
          this.movies = data['content'];
          this.totalPages = new Array(data['totalPages']);
        }
      );

    });

  }

  private getNextPage() {
    return parseInt(this.page) + 1;
  }

}
