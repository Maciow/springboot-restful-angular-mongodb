export interface Actor {
  firstName: string;
  lastName: string;
  birthday: string;
}
