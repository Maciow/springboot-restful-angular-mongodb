import {Country} from './country';
import {Actor} from './actor';
import {Vote} from './vote';
import {Genre} from './genre';

export interface Movie {
  id?: string;
  title: string;
  length: number;
  releaseDate: string;
  countries: Country[];
  director: string;
  description:  string;
  rating: number;
  actors: Actor[];
  votes: Vote[];
  genres: Genre[];
}
