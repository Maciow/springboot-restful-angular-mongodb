export class Vote {
  username: string;
  voteValue: number;
  movieId: string;

  constructor(username: string, voteValue: number, movieId: string) {
    this.username = username;
    this.voteValue = voteValue;
    this.movieId = movieId;
  }
}
