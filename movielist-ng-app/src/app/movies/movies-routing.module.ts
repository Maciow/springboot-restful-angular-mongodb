import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListMovieComponent} from './list-movie/list-movie.component';
import {MovieDetailComponent} from './movie-detail/movie-detail.component';
import {AddMovieComponent} from './add-movie/add-movie.component';
import {AddMovieGuard} from '../guard/add-movie-guard';

const moviesRoutes: Routes = [
  {path: 'movies', component: ListMovieComponent},
  {path: 'movies/:title/:id', component: MovieDetailComponent},
  {path: 'movies/add', component: AddMovieComponent, canActivate: [AddMovieGuard]}
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    RouterModule.forChild(moviesRoutes)
  ]
})
export class MoviesRoutingModule { }
