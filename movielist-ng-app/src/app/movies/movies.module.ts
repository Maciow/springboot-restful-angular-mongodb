import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {MovieDetailComponent} from './movie-detail/movie-detail.component';
import {ListMovieComponent} from './list-movie/list-movie.component';
import {AddMovieComponent} from './add-movie/add-movie.component';
import {MoviesRoutingModule} from './movies-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    MovieDetailComponent,
    ListMovieComponent,
    AddMovieComponent,
  ],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class MoviesModule { }
