import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Movie} from '../model/movie';
import {MovieService} from '../../service/movie.service';
import {AuthenticationService} from '../../service/authentication.service';
import {Vote} from '../model/vote';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  private voteInfoList: Array<string>;
  private totalRatings: Array<number>;
  private voteMessage: string;
  private votePicked: number;
  private movie: Movie;
  voteInfo: string;

  constructor(private route: ActivatedRoute, private router: Router, private authService: AuthenticationService, private movieService: MovieService) { }

  ngOnInit() {
    this.totalRatings = new Array<number>(10);
    this.voteInfoList = ['Misunderstanding (1)', 'Very bad (2)', 'Weak (3)', 'Nothing special (4)',
                        'Average (5)', 'Not bad (6)', 'Good (7)', 'Very good (8)',
                        'Sensational (9)', 'Masterpiece (10)', 'I saw and my rate is:'];
    this.voteInfo = this.voteInfoList[10];
    this.getMovieFromParams();
    this.votePicked = 0;
  }

  private getMovieFromParams() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.movieService.getMovieByTitleAndId(
        params.get('id'), params.get('title'))
      )
    ).subscribe(data => {
      this.movie = data;
      this.getPickedVote();
    });
  }

  private vote(voteValue: number, movieId: string) {
    let username = this.authService.getUsernameFromToken();
    if (username) {
      let vote: Vote = new Vote(username, voteValue, movieId);
      this.votePicked = vote.voteValue;
      this.movieService.voteForMovie(vote).subscribe(data => {
        this.voteMessage = data.messages.pop();
        this.getMovieFromParams();
      });
    } else {
      this.router.navigate(['/login']);
    }
  }

  private getPickedVote() {
    let username = this.authService.getUsernameFromToken();
    if (username && this.movie.id) {
      let params = new HttpParams()
        .set('username', username)
        .set('movieId', this.movie.id);
      this.movieService.checkPickedVote(params).subscribe(data => {
        if (data.status !== 404) {
          this.votePicked = data.result;
        }
      });
    } else {
      this.votePicked = 0;
    }
  }
}
