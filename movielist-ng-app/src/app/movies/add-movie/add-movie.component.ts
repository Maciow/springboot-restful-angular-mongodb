import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormArray, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {MovieService} from '../../service/movie.service';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private movieService: MovieService) { }

  addMovieForm: FormGroup;
  countries: FormArray;
  actors: FormArray;
  genres: FormArray;
  errorMessages: Array<string>;

  ngOnInit() {
    this.addMovieForm = this.formBuilder.group({
      title: ['', Validators.required],
      length: ['', [Validators.required, Validators.min(1)]],
      releaseDate: ['', [Validators.required, this.validateDate]],
      countries: this.formBuilder.array([this.createCountry()]),
      director: ['', [Validators.required, this.validatePeopleNames]],
      description: ['', [Validators.required, Validators.maxLength(700)]],
      actors: this.formBuilder.array([this.createActor()]),
      genres: this.formBuilder.array([this.createGenre()]),
    });
    this.errorMessages = [];
  }

  onSubmit() {
    this.movieService.saveMovie(this.addMovieForm.value).subscribe(
      data => {
        if (data.status === 201) {
          this.router.navigate(['movies']);
        } else {
          this.errorMessages = data.messages;
        }
      }
    );
  }

  createCountry(): FormGroup {
    return this.formBuilder.group({
      name: ['', [Validators.required, this.validateCountryName]]
    });
  }

  addCountry(): void {
    this.countries = this.addMovieForm.get('countries') as FormArray;
    this.countries.push(this.createCountry());
  }

  removeCountry(i: number) {
    this.countries.removeAt(i);
  }

  createActor() {
    return this.formBuilder.group({
      firstName: ['', [Validators.required, this.validatePeopleNames]],
      lastName: ['', [Validators.required, this.validatePeopleNames]],
      birthday: ['', [Validators.required, this.validateDate]]
    });
  }

  addActor(): void {
    this.actors = this.addMovieForm.get('actors') as FormArray;
    this.actors.push(this.createActor());
  }

  removeActor(i: number) {
    this.actors.removeAt(i);
  }

  createGenre() {
    return this.formBuilder.group({
      genre: ['', Validators.required]
    });
  }

  addGenre(): void {
    this.genres = this.addMovieForm.get('genres') as FormArray;
    this.genres.push(this.createGenre());
  }

  removeGenre(i: number) {
    this.genres.removeAt(i);
  }

  private validateDate(control: FormControl) {
    const DATE_REGEXP: RegExp = new RegExp(/^((0?[1-9]|1[012])[-](0?[1-9]|[12][0-9]|3[01])[-](19|20)?[0-9]{2})*$/);
    return DATE_REGEXP.test(control.value) ? null : {
      validateDate: {
        valid: false
      }
    };
  }

  private validateCountryName(control: FormControl) {
    const COUNTRY_REGEXP: RegExp = new RegExp(/^[a-zA-Z]+$/);
    return COUNTRY_REGEXP.test(control.value) ? null : {
      validateCountryName: {
        valid: false
      }
    };
  }

  private validatePeopleNames(control: FormControl) {
    const NAME_REGEXP: RegExp = new RegExp(/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u);
    return NAME_REGEXP.test(control.value) ? null : {
      validatePeopleNames: {
        valid: false
      }
    };
  }

}
