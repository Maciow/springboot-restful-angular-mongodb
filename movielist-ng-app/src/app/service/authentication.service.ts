import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private BASE_URL = 'http://localhost:8080';
  public adminCheck: Subject<boolean> = new BehaviorSubject<boolean>(null);
  public loggedCheck: Subject<boolean> = new BehaviorSubject<boolean>(null);

  constructor(private http: HttpClient, private router: Router) { }

  login(loginPayload) {
    const headers = {
      'Authorization': 'Basic ' + btoa('movielist-client:movielistsecret'),
      'Content-type': 'application/x-www-form-urlencoded'
    };
    return this.http.post(this.BASE_URL + '/oauth/token', loginPayload, {headers});
  }

  logout() {
    window.localStorage.removeItem('token');
    this.router.navigate(['movies']);
    this.adminCheck.next(false);
    this.loggedCheck.next(false);
  }

  getToken(): string {
    return window.localStorage.getItem('token');
  }

  isAdmin(): boolean {
    const decodedJwtData = this.getDecodedJwtToken();
    return decodedJwtData.authorities.includes('ROLE_ADMIN');
  }

  getUsernameFromToken() {
    const decodedJwtToken = this.getDecodedJwtToken();
    if (decodedJwtToken) {
      return decodedJwtToken.user_name;
    } else {
      return null;
    }
  }

  private getDecodedJwtToken() {
    const token = this.getToken();
    if (token) {
      const tokenData = token.split('.')[1];
      const decodedTokenJsonData = window.atob(tokenData);
      return JSON.parse(decodedTokenJsonData);
    } else {
      return null;
    }
  }
}
