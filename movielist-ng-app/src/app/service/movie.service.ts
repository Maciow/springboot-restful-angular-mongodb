import { Injectable } from '@angular/core';
import {Movie} from '../movies/model/movie';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ApiResponse} from '../user/model/api-response';
import {Vote} from '../movies/model/vote';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private BASE_URL = 'http://localhost:8080';
  private ALL_MOVIES_URL = `${this.BASE_URL}/movies`;
  private VOTE_FOR_MOVIE = `${this.BASE_URL}/movies/vote`;

  constructor(private http: HttpClient) { }

  saveMovie(movie: Movie): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.ALL_MOVIES_URL, movie);
  }

  getAllMovies(params): Observable<any> {
    return this.http.get<any>(this.ALL_MOVIES_URL, {params});
  }

  getMovieByTitleAndId(id: string, title: string): Observable<Movie> {
    return this.http.get<Movie>(this.ALL_MOVIES_URL + '/' + id + '/' + title);
  }

  voteForMovie(vote: Vote): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.VOTE_FOR_MOVIE, vote);
  }

  checkPickedVote(params: HttpParams): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.VOTE_FOR_MOVIE, {params});
  }
}

