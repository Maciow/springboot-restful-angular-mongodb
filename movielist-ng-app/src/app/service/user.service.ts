import { Injectable } from '@angular/core';
import {User} from '../user/model/user';
import {AsyncSubject, BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ApiResponse} from '../user/model/api-response';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private BASE_URL = 'http://localhost:8080';
  private REGISTER_USER_URL = `${this.BASE_URL}/register`;
  private IS_USER_USERNAME_EXISTS = `${this.BASE_URL}/user/username/`;
  private IS_USER_EMAIL_EXISTS = `${this.BASE_URL}/user/email/`;

  constructor(private http: HttpClient) { }

  saveUser(user: User): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.REGISTER_USER_URL, user);
  }

  isUsernameExists(username: string): Observable<boolean> {
    return this.http.get<boolean>(this.IS_USER_USERNAME_EXISTS + username);
  }
  isEmailExists(email: string): Observable<boolean> {
    return this.http.get<boolean>(this.IS_USER_EMAIL_EXISTS + email);
  }
}
