package com.movielist.controller;

import com.movielist.model.ApiResponse;
import com.movielist.model.Movie;
import com.movielist.model.VoteDTO;
import com.movielist.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@CrossOrigin
@RequestMapping("/movies")
public class MovieController {

    private MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping
    public Page<Movie> getAll(@RequestParam(defaultValue = "1") int page,
                              @RequestParam(defaultValue = "5") int size,
                              @RequestParam(defaultValue = "0") double minRate,
                              @RequestParam(required = false) String genres){
        return this.movieService.getAllMovies(genres,minRate,page-1, size);
    }

    @GetMapping("/{id}/{title}")
    public Movie getMovieByIdAndTitle(@PathVariable String id,
                                      @PathVariable String title){

        return this.movieService.getMovieByIdAndTitle(id,title);
    }

    @PostMapping
    public ResponseEntity<ApiResponse<Movie>> saveMovie(@RequestBody final Movie movie){
        ApiResponse<Movie> response = this.movieService.saveMovie(movie);
        if (response.getStatus() == 201) {
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}/{title}")
                    .buildAndExpand(response.getResult().getId(),response.getResult().getTitle())
                    .toUri();
            return ResponseEntity.created(location).body(response);
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
        }
    }

    @PostMapping("/vote")
    public ResponseEntity<ApiResponse<VoteDTO>> voteForMovie(@RequestBody final VoteDTO vote) {
        ApiResponse<VoteDTO> response = this.movieService.voteForMovie(vote);
        if (response.getStatus() == 201) {
            return ResponseEntity.status(201).body(response);
        } else {
            return ResponseEntity.status(response.getStatus()).body(response);
        }
    }

    @GetMapping("/vote")
    public ApiResponse<Integer> getPickedVote(@RequestParam String username,
                                              @RequestParam String movieId) {
        return this.movieService.getPickedVote(username, movieId);
    }

    @PutMapping("/{id}")
    public void updateMovie(@RequestBody Movie movie, @PathVariable String id){
        this.movieService.updateMovie(movie,id);
    }

    @DeleteMapping("/{id}")
    public void deleteMovie(@PathVariable String id){
        this.movieService.deleteMovie(id);
    }
}
