package com.movielist.controller;

import com.movielist.model.ApiResponse;
import com.movielist.model.User;
import com.movielist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ApiResponse<User> saveUser(@RequestBody final User user){
        return this.userService.saveUser(user);
    }

    @GetMapping("/user/email/{email}")
    public boolean isUserWithEmailExists(@PathVariable String email) {
        return this.userService.findUserByEmail(email) != null;
    }

    @GetMapping("/user/username/{username}")
    public boolean isUserWithUsernameExists(@PathVariable String username) {
        return this.userService.findUserByUsername(username) != null;
    }

    @GetMapping("/activatelink/{code}")
    public void confirmUser(@PathVariable String code){
        this.userService.updateConfirmation(code);
    }
}
