package com.movielist.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserRegistrationValidator {

    private static final String INVALID_USERNAME_EXISTS = "User with this username already exists";
    private static final String INVALID_USERNAME_LENGTH = "Username must be between 4 and 12 characters long";
    private static final String INVALID_EMAIL_EXISTS = "User with this email already exists";
    private static final String INVALID_EMAIL_PATTERN = "Invalid email pattern";
    private static final String INVALID_CONFIRM_PASSWORD = "Passwords are not the same";
    private static final String INVALID_PASSWORD_LENGTH = "Password must be between 4 and 12 characters long";
    private static final String EMAIL_PATTERN = "^[a-zA-z0-9]+[\\._a-zA-Z0-9]*@[a-zA-Z0-9]+{2,}\\.[a-zA-Z]{2,}[\\.a-zA-Z0-9]*$";

    public String validateUsername(String username, boolean isUserExists) {
        if (username.length() > 3 && username.length() < 12){
            if (!isUserExists){
                return null;
            } else return INVALID_USERNAME_EXISTS;
        } else return INVALID_USERNAME_LENGTH;
    }

    public String validateEmail(String email, boolean isUserExists) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()){
            if (!isUserExists) {
                return null;
            } else return INVALID_EMAIL_EXISTS;
        } else return INVALID_EMAIL_PATTERN;
    }

    public String validatePasswords(String password, String confirmPassword){
        if (password.length() > 3 && password.length() < 13) {
            if (!password.equals(confirmPassword)) {
                return INVALID_CONFIRM_PASSWORD;
            } else return null;
        } else return INVALID_PASSWORD_LENGTH;
    }
}
