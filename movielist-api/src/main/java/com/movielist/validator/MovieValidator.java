package com.movielist.validator;

import com.movielist.model.Country;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MovieValidator {

    private final static String INVALID_TITLE_EMPTY = "Title cannot be empty";
    private final static String INVALID_LENGTH = "Length cannot be empty or less than 1 minute";
    private final static String INVALID_COUNTRY = "Country %s is invalid";
    private final static String INVALID_PEOPLE_NAMES = "Name is invalid";
    private final static String INVALID_DESCRIPTION_LENGTH = "Description cannot be longer than 700 characters";
    private final static String INVALID_GENRE_EMPTY = "Genre cannot be empty";

    private final static String DATE_PATTERN = "^((0?[1-9]|1[012])[-](0?[1-9]|[12][0-9]|3[01])[-](19|20)?[0-9]{2})*$";
    private final static String COUNTRY_PATTERN = "^[a-zA-Z]+$";
    private final static String PEOPLE_NAMES_PATTERN = "^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$";

    public String validateTitle(String title) {
        if (title != null){
            return null;
        } else return INVALID_TITLE_EMPTY;
    }

    public String validateLength(Integer length) {
        if (length != null && length > 0) {
            return null;
        } else return INVALID_LENGTH;
    }

    public String validateCountries(List<Country> countries) {
        Pattern pattern = Pattern.compile(COUNTRY_PATTERN);
        for (Country country : countries) {
            Matcher matcher = pattern.matcher(country.getName());
            if (!matcher.matches()) {
                return String.format(INVALID_COUNTRY, country.getName());
            }
        }
        return null;
    }

    public String validatePeopleNames(List<String> names) {
        Pattern pattern = Pattern.compile(PEOPLE_NAMES_PATTERN);
        for (String name: names){
            Matcher matcher = pattern.matcher(name);
            if (!matcher.matches()) {
                return INVALID_PEOPLE_NAMES;
            }
        }
        return null;
    }

    public String validateDescription(String description) {
        if (description.length() <= 700){
            return null;
        } else return INVALID_DESCRIPTION_LENGTH;
    }

    public String validateGenre(List<String> genres) {
        if (!genres.isEmpty()) {
            return null;
        } return INVALID_GENRE_EMPTY;
    }
}
