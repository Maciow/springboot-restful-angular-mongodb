package com.movielist.repository;

import com.movielist.model.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends MongoRepository<Movie, String>, QuerydslPredicateExecutor<Movie> {
    Movie findMovieByIdAndTitle(String id, String title);
    Page<Movie> findAll(Pageable pageable);
}
