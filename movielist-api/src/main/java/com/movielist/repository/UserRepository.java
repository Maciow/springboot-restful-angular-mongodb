package com.movielist.repository;

import com.movielist.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String>, QuerydslPredicateExecutor<User> {
    User findByUsername(String username);
    User findByActivationCode(String code);
    User findByEmail(String email);
}
