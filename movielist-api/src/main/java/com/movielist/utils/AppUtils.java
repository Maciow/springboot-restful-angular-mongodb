package com.movielist.utils;

import java.util.Random;

public class AppUtils {

    public static String activationCodeGenerator(){
        StringBuilder builder = new StringBuilder();

        String signs = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        Random random = new Random();

        for (int i=0; i<32; i++){
            int number = random.nextInt(signs.length());
            builder.append(signs,number,number+1);
        }
        return builder.toString();
    }
}
