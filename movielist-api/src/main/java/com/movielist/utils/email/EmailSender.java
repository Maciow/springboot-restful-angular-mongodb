package com.movielist.utils.email;

public interface EmailSender {
    void sendEmail(String to, String subject, String text);
}
