package com.movielist.model;

public class Vote {

    private String userId;
    private Integer rating;

    public Vote() {
    }

    public Vote(String userId, Integer rating) {
        this.userId = userId;
        this.rating = rating;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
