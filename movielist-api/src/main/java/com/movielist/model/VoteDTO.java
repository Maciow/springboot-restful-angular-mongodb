package com.movielist.model;

public class VoteDTO {

    private String username;
    private Integer voteValue;
    private String movieId;

    public VoteDTO() {
    }

    public VoteDTO(String username, Integer voteValue, String movieId) {
        this.username = username;
        this.voteValue = voteValue;
        this.movieId = movieId;
    }

    public String getMovieId() {
        return movieId;
    }

    public String getUsername() {
        return username;
    }

    public Integer getVoteValue() {
        return voteValue;
    }
}
