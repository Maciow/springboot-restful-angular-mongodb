package com.movielist.model;

import java.util.Set;

public class ApiResponse<T> {

    private Integer status;
    private Set<String> messages;
    private T result;

    public ApiResponse(Integer status, Set<String> messages, T result) {
        this.status = status;
        this.messages = messages;
        this.result = result;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Set<String> getMessages() {
        return messages;
    }

    public void setMessages(Set<String> messages) {
        this.messages = messages;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
