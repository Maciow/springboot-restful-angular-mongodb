package com.movielist.service;

import com.movielist.model.*;
import com.movielist.repository.MovieRepository;
import com.movielist.validator.MovieValidator;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class MovieService {

    private final static String SUCCESSFUL_ADD_MOVIE = "Movie has created successfully";

    private MovieRepository movieRepository;
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setMovieRepository(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public Page<Movie> getAllMovies(String genres,
                                    double minRate,
                                    int page,
                                    int size){
        QMovie qMovie = new QMovie("movie");
        BooleanExpression filterByRate = qMovie.rating.goe(minRate);
        if (genres!=null && !genres.equals("null")){
            BooleanExpression filterByGenre = qMovie.genres.any().genre.eq(genres);
            return this.movieRepository.findAll(filterByRate.and(filterByGenre),PageRequest.of(page,size));
        } else {
            return this.movieRepository.findAll(filterByRate,PageRequest.of(page,size));
        }
    }

    public Movie getMovieByIdAndTitle(String id, String title){
        return this.movieRepository.findMovieByIdAndTitle(id,title);
    }

    public ApiResponse<Movie> saveMovie(final Movie movie){
        Set<String> errorMessages = getErrorMessages(movie);
        if (errorMessages.size() == 0){
            this.movieRepository.save(movie);
            return new ApiResponse<>(201,Collections.singleton(SUCCESSFUL_ADD_MOVIE),movie);
        } else {
            return new ApiResponse<>(409,errorMessages,movie);
        }
    }

    public ApiResponse<VoteDTO> voteForMovie(final VoteDTO vote) {
        final User user = this.userService.findUserByUsername(vote.getUsername());
        final Movie movie = this.movieRepository.findById(vote.getMovieId()).orElse(null);
        if (user != null && movie != null) {
            boolean isVoteExists = movie.getVotes()
                    .stream()
                    .map(Vote::getUserId)
                    .anyMatch(userId -> userId.equals(user.getId()));
            if (!isVoteExists) {
                movie.getVotes().add(new Vote(user.getId(),vote.getVoteValue()));
                movie.setRating(movie.getVotes().stream().mapToDouble(Vote::getRating).average().orElse(0.0));
                this.movieRepository.save(movie);
                return new ApiResponse<>(201,Collections.singleton("You have voted successfully"), vote);
            } else {
                Vote oldVote = movie.getVotes().stream().filter(voteFiltered -> voteFiltered.getUserId().equals(user.getId())).findFirst().get();
                movie.getVotes().remove(oldVote);
                movie.getVotes().add(new Vote(user.getId(), vote.getVoteValue()));
                movie.setRating(movie.getVotes().stream().mapToDouble(Vote::getRating).average().orElse(0.0));
                this.movieRepository.save(movie);
                return new ApiResponse<>(201,Collections.singleton("Your vote has updated successfully"), vote);
            }
        } else {
            return new ApiResponse<>(409, Collections.singleton("Cannot find movie or user"), vote);
        }
    }

    public ApiResponse<Integer> getPickedVote(String username, String movieId) {
        final User user = this.userService.findUserByUsername(username);
        final Movie movie = this.movieRepository.findById(movieId).orElse(null);
        if (user != null && movie != null) {
            Integer voteValue = movie.getVotes().stream().filter(vote -> vote.getUserId().equals(user.getId())).map(Vote::getRating).findFirst().orElse(null);
            if (voteValue != null){
                return new ApiResponse<>(302, Collections.singleton("Value of the vote has been founded"), voteValue);
            } else {
                return new ApiResponse<>(404, Collections.singleton("Cannot find value of the vote"), null);
            }
        } else {
            return new ApiResponse<>(404, Collections.singleton("Cannot find value of the vote"), null);
        }
    }

    private Set<String> getErrorMessages(final Movie movie) {
        Set<String> errorMessages = new HashSet<>();
        errorMessages.add(validateTitle(movie.getTitle()));
        errorMessages.add(validateLength(movie.getLength()));
        errorMessages.add(validateCountries(movie.getCountries()));
        errorMessages.add(validatePeopleNames(Collections.singletonList(movie.getDirector())));
        errorMessages.add(validatePeopleNames(movie.getActors().stream().map(Actor::getFirstName).collect(Collectors.toList())));
        errorMessages.add(validatePeopleNames(movie.getActors().stream().map(Actor::getLastName).collect(Collectors.toList())));
        errorMessages.add(validateDescription(movie.getDescription()));
        errorMessages.add(validateGenre(movie.getGenres().stream().map(Genre::getGenre).collect(Collectors.toList())));
        errorMessages.remove(null);
        return errorMessages;
    }

    public void updateMovie(Movie updatedMovie, String id){
        this.movieRepository.findById(id).ifPresent(movie -> {
            if (updatedMovie.getTitle() != null){
                movie.setTitle(updatedMovie.getTitle());
            }
            if (updatedMovie.getLength() != null) {
                movie.setLength(updatedMovie.getLength());
            }
            if (updatedMovie.getReleaseDate() != null){
                movie.setReleaseDate(updatedMovie.getReleaseDate());
            }
            if (updatedMovie.getCountries() != null && !updatedMovie.getCountries().isEmpty()){
                movie.getCountries().addAll(updatedMovie.getCountries());
            }
            if (updatedMovie.getDirector() != null){
                movie.setDirector(updatedMovie.getDirector());
            }
            if (updatedMovie.getDescription() != null){
                movie.setDescription(updatedMovie.getDescription());
            }
            if (updatedMovie.getVotes() != null && !updatedMovie.getVotes().isEmpty()){
                movie.getVotes().addAll(updatedMovie.getVotes());
                movie.setRating(movie.getVotes().stream().mapToDouble(Vote::getRating).average().orElse(0.0));
            }
            if (updatedMovie.getGenres() != null){
                movie.getGenres().addAll(updatedMovie.getGenres());
            }
            this.movieRepository.save(movie);
        });
    }

    public void deleteMovie(String id){
        this.movieRepository.deleteById(id);
    }

    private String validateTitle(String title){
        return new MovieValidator().validateTitle(title);
    }

    private String validateLength(Integer length){
        return new MovieValidator().validateLength(length);
    }

    private String validateCountries(List<Country> countries){
        return new MovieValidator().validateCountries(countries);
    }

    private String validatePeopleNames(List<String> names){
        return new MovieValidator().validatePeopleNames(names);
    }

    private String validateDescription(String description) {
        return new MovieValidator().validateDescription(description);
    }

    private String validateGenre(List<String> genres) {
        return new MovieValidator().validateGenre(genres);
    }
}
