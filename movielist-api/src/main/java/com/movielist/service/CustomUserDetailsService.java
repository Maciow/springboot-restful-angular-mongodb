package com.movielist.service;

import com.movielist.model.User;
import com.movielist.model.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userModel = userService.findUserByUsername(username);
        if (userModel == null){
            throw new UsernameNotFoundException("There is no such user");
        } else if (!userModel.getConfirmed()){
            throw new UsernameNotFoundException("You have to open the activation link in your email");
        }
        return new org.springframework.security.core.userdetails.User(
                userModel.getUsername(),
                userModel.getPassword(),
                convertAuthorities(userModel.getRoles())
        );
    }

    private Set<GrantedAuthority> convertAuthorities(Set<UserRole> roles) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (UserRole r: roles){
            authorities.add(new SimpleGrantedAuthority(r.getRole()));
        }
        return authorities;
    }
}
