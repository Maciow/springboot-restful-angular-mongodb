package com.movielist.service;

import com.movielist.model.ApiResponse;
import com.movielist.model.User;
import com.movielist.model.UserRole;
import com.movielist.repository.UserRepository;
import com.movielist.utils.AppUtils;
import com.movielist.utils.email.EmailSender;
import com.movielist.validator.UserRegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserService {
    private final static String DEFAULT_ROLE = "ROLE_USER";
    private final static String DEFAULT_ROLE_DESCRIPTION = "Default role for user";
    private final static String SUCCESSFUL_REGISTRATION = "User has registered successfully";
    private BCryptPasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private EmailSender emailSender;

    @Autowired
    public void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    public User findUserByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public User findUserByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }

    public ApiResponse<User> saveUser(final User user){
        Set<String> errorMessages = getErrorMessages(user);
        if (errorMessages.size() == 0){
            return createUser(user);
        } else {
            return new ApiResponse<>(409,errorMessages, user);
        }
    }

    private Set<String> getErrorMessages(final User user){
        Set<String> errorMessages = new HashSet<>();
        errorMessages.add(validateUsername(user.getUsername()));
        errorMessages.add(validateEmail(user.getEmail()));
        errorMessages.add(validatePasswords(user.getPassword(), user.getConfirmPassword()));
        errorMessages.remove(null);
        return errorMessages;
    }

    private ApiResponse<User> createUser(User user) {
        user.setActivationCode(AppUtils.activationCodeGenerator());
        user.setConfirmed(false);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.getRoles().add(new UserRole(DEFAULT_ROLE,DEFAULT_ROLE_DESCRIPTION));
        this.userRepository.save(user);
        sendEmail(user,emailSender);
        return new ApiResponse<>(201, Collections.singleton(SUCCESSFUL_REGISTRATION), user);
    }

    private static void sendEmail(User user, EmailSender emailSender){
        String text = "Open the link to confirm your account \n. http://localhost:8080/activatelink/"+user.getActivationCode();
        emailSender.sendEmail(user.getEmail(),"Confirm your account", text);
    }

    public void updateConfirmation(String activationCode){
        User user = this.userRepository.findByActivationCode(activationCode);
        if (!user.getConfirmed()){
            user.setConfirmed(true);
            this.userRepository.save(user);
        }
    }

    private String validateUsername(String username) {
        User user = this.userRepository.findByUsername(username);
        boolean isUserExists = user != null;
        return new UserRegistrationValidator().validateUsername(username, isUserExists);
    }

    private String validateEmail(String email) {
        User user = this.userRepository.findByEmail(email);
        boolean isUserExists = user != null;
        return new UserRegistrationValidator().validateEmail(email, isUserExists);
    }

    private String validatePasswords(final String password, final String confirmPassword){
        return new UserRegistrationValidator().validatePasswords(password, confirmPassword);
    }
}
